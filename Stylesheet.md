[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt

# Afonso Stylesheet Style Guide and Best Practices
---

This document serves as a guideline for developers working with and for Afonso. All code delivered to Afonso MUST follow this guide. Third party libraries are excluded from this guide.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119][].

## General guidelines

The REQUIRED preprocessor for all stylesheet generation is Sass https://en.wikipedia.org/wiki/Sass_(stylesheet_language)

### Block, Element, Modifier Pattern

All code SHOULD follow the BEM-principle. _Example below_

```sass
.account {
	&__avatar {
		&&--pro {
			# Use double ampersand to generate inheritance.
			# Pro user avatars
		}
	}
}
```

```html
<div class="account__avatar account__avatar--pro"></div>
```

### Naming

- All classes and IDs must have names that are relevant, descriptive, _make sense_ and can easily be understood.

**Good**

```html
<div class="header"></div>
<div class="footer"></div>
```

**Bad**

```html
<div class="c3"></div>
<div class="__b7"></div>
```

## Indentation and spaces

- All code MUST be indentated with hard tabs.
- Statements, rules, and media queries MUST be properly spaced with line breaks.
- Properties and values MUST be separated with a single space after the `:`.

**Good**

```sass
.class-name {
	color: #fff;

	&--modifier {
		color: gold;
	}

	@media (max-width: 1024px) {
		strong {
			font-weight: bold;
		}
	}
}
```

**Bad**

```sass
.class-name {
	color:#fff;
	&--modifier {
		color:gold;
	}
	@media (max-width: 1024px) {
		strong {
			font-weight:bold;
		}
	}
}
```

## Brackets

- Opening bracket MUST be on the same row as it's statement.

**Good**

```sass
.class-name {
	color: #fff;
}
```

**Bad**

```sass
.class-name
{
	color: #fff;
}
```

## CSS reset

- All sites SHOULD use Eric Meyers CSS reset. It can be found here: https://meyerweb.com/eric/tools/css/reset/
- All code MUST use `border-box` (preferably add this right after the CSS reset code: `* { box-sizing: border-box; }`).
- Properties SHOULD NOT be reset/set using the `*` selector.

**Good**

```sass
@import "reset";

body {
	color: gold;
}

```

**Bad**

```sass
* {
	line-height: 1;
	padding: 0;
	margin: 0;
}

body {
	color: gold;
}

@import "reset";
```
