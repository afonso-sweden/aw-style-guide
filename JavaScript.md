[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt

# Afonso JavaScript Style Guide and Best Practices
---

This document serves as a guideline for developers working with and for Afonso. All code delivered to Afonso MUST follow this guide. Third party libraries are excluded from this guide.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119][].

## General guidelines

It is RECOMMENDED that all code is well documented and verbose. Class, variable and function names must be descriptive. _Example below_

**Good**

```javascript
class EventEmitter {

	function emit(eventName, ...eventCallbackArguments) {
		/* Codes */
	}

}
```

**Bad**

```javascript
class CallbackHandlerClass {

	function invoke(name, ...options) {
		/* Codes */
	}

}
```

## Indentation

All code MUST be indentated with hard tabs.

## Case

Variable and function names MUST be in camelCase, private or anonymous variables or functions MAY use a single underscore as prefix. Class names MUST always be PascalCased.

## Quotes

You SHOULD use single quotes.

**Good**

```javascript
var bar = 'baz';
```

**Bad**

```javascript
var bar = "baz";
```

## Variables

- Variable names MUST be camelCase.

## Functions

### Defining functions

- A space MUST supersede the closing parentheses.
- The opening bracket MUST be on the same row as the function declaration.
- Function names MUST be camelCase.
- Function definitions MAY be defined as function variables (_example below_).

**Good**

```javascript
function fooBar(baz) {
	'use strict';
}
```

**Bad**

```javascript
function foo_bar(baz)
{
	'use strict';
}
```

**Variable function definition**

```javascript
var fooBar = function(baz) {
	'use strict';
};
```

## Semi Colons
- You MUST use semi colons.

**Good**

```javascript
function fooBar(baz) {
	'use strict';
	fooBar('baz');
}
```

**Bad**

```javascript
function foo_bar(baz)
{
	'use strict'
	fooBar('baz')
}
```

## Brackets
- Opening bracket MUST be on the same row as it's statement.

**Good**

```javascript
if (foo) {
	bar(baz);
}
```

**Bad**

```javascript
if (foo)
{
	bar(baz);
}
```

## Strict mode
All code MUST be in strict mode. Encapsulate all code in functions to avoid setting the entire script to strict mode.

```javascript
(function() {
	'use strict';
	foo(bar);
})();
```

## React Components

It is RECOMMENDED that you write all React components in `ES2015` style. _Example below_

```javascript
class SomeAwesomeComponent extends React.Component {

	render() {
		return <h1>Awesome!</h1>;
	}

}
```
