[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt

# Afonso Code Style Guide and Best Practices
---

This document serves as a guideline for developers working with and for Afonso. All code delivered to Afonso MUST follow this guide. Third party libraries are excluded from this guide.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119][].

## Indentation

- All code MUST be indented with tabs.
- You MAY use spaces for alignment. (_Copy example into an editor to get a better understanding for it_).

**Example**

```javascript
(function() {
	var foo = 'bar',
			bar = 'baz',
			varName = {
				obj: 1
			};
});
```

- This applies to **ALL** languages unless otherwise specified under its own section.

## Version control

We use Git for version control. Currently all repositories are hosted on Bitbucket. You will be given access to the appropriate repositories as you work on them.

### Branches

- Every project MUST have two main branches, `master` and `dev`.
- `master` is only meant for code that runs in production.
- `dev` is meant for code that is finished, but needs testing.
- All features MUST be developed in feature branches, branched out from `dev`.
- Feature branches SHOULD continuously be kept up to date with the dev branch, to simplify integrating the work when multiple people are working on the project.
- Emergency fixes, so called hotfixes, MUST be done in hotfix branches branched out from `master`. After they are completed, they MUST be merged back into both `master` and `dev`.
- Hotfix branches MUST follow the naming convention `hotfix-*`.
- After a feature is completed, tested, and accepted for release on the `dev` branch it MUST be merged into `master`, and the `master` branch updated on the production server to reflect the change.
- Every website/application MUST run with the `master` branch checked out on production servers, no exceptions.

### Commits

- Commit messages MUST be descriptive.
Examples of bad commit messages are "Fix", "Adjust", or other one/few word messages that do not describe the changes.
- Commits SHOULD be done in small iterations, each commit covering one "deliverable" feature. This makes it easy to roll back commits without breaking code. For example, do not update more than one plugin in one single commit.

### Gitignore
Add `node_modules`, `wp-config.php`, log files, and other files (or folders) that should be ignored in `.gitignore`. This may be different files (or folders) depending on the project/site/app. For example, on our Siteground servers you must add `php_errorlog` in `.gitignore`.

### Git on servers
All accounts/sites on production and dev servers that uses git, MUST have git set up WITHOUT a personal username. See examples below.

**Good**

```
[remote "origin"]
	url = https://bitbucket.org/username/site.git
```

**Bad**

```
[remote "origin"]
	url = https://username@bitbucket.org/username/site.git
```

## WordPress

- All production sites MUST have `DISALLOW_FILE_MODS` and `DISALLOW_FILE_EDIT` defined and set to `true` in wp-config.php.

```php
<?php
define('DISALLOW_FILE_MODS', true);
define('DISALLOW_FILE_EDIT', true);
```

## Servers

All accounts/sites on our Siteground production and dev servers MUST:
- Have file editing in WordPress turned off (see above for code example).
- Have PHP version changed to 7 or above.
- A file in the root called .vimrc with the content below:
```
set nocompatible
set backspace=indent,eol,start
set nu
```

## Language Specific Guides

Language specific guides are found as sub-documents in this repository.
