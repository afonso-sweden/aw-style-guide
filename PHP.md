[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt

# Afonso PHP Style Guide and Best Practices
---

This document serves as a guideline for developers working with and for Afonso. All code delivered to Afonso MUST follow this guide. Third party libraries are excluded from this guide.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119][].



## General guidelines

It is RECOMMENDED that all code is well documented and verbose. Classes, abstracts, interfaces, traits, variable and function names must be descriptive. _Example below_

**Good**

```php
<?php
class EventEmitter {

	public function emit($eventName, Array $eventCallbackArguments) {
		/* Codes */
	}

}
```

**Bad**

```php
<?php
class CallbackHandlerClass {

	function invoke($name, $options) {
		/* Codes */
	}

}
```

ALWAYS check if a variable exists (or is not NULL, etc.) before trying to use it in a way that will generate an error or warning. _Example below_

**Good**

```php
<?php
$heading = get_field('heading');

if ($heading) {
	echo $heading;
}

// or

echo $heading ? $heading : 'A default heading...';
```

**Bad**

```php
<?php
$heading = get_field('heading');

// This will generate (and log) a warning if heading is NULL.
echo $heading;
```

## Type hinting
Type hinting is *OPTIONAL* for PHP versions below 7.0. In versions above and matching 7.0 it is *RECOMMENDED*. This includes return type hinting.

```php
<?php
class Greeter {

	protected $friends = [
		'Bruce'
	];

	function greet(String $name):String {
		return sprintf("Hello %s!", $name);
	}

	function greetAll(String ...$names):String {
		return sprintf("Hello %s!", implode(', ', $names));
	}

	function canGreet(String $name):Bool {
		return (in_array($name, $this->friends) === true);
	}

}

$greeter = new Greeter;
echo $greeter->greet('Harley');
echo $greeter->greetAll('Bruce', 'Dick', 'Jason');
echo ($greeter->canGreet('Bruce')) ? 'Hiya Batman!' : '...';
```

## Indentation

All code MUST be indented with hard tabs.

## Case

Variable and function names MUST be in camelCase, private or anonymous variables or functions MAY use a single underscore as prefix. Classes, Abstracts, Interfaces and Traits names MUST always be PascalCased.

## Quotes

You SHOULD use single quotes where strings are _not_ interpolated. Concatenation is _acceptable_. For long interpolated strings use `sprintf` or `implode` for sub-optimizations.

## Variables

- Variable names MUST be camelCase.

## Functions

### Defining functions

- A space MUST supersede the closing parentheses.
- The opening bracket MUST be on the same row as the function declaration.
- Function names MUST be camelCase.
- Function definitions MAY be defined as function variables (_example below_).

**Good**

```php
<?php
function fooBar($baz) {
	return $baz;
}
```

**Bad**

```php
<?php
function foo_bar($baz)
{
	return $baz;
}
```

### Return statements
Functions *MUST NOT* use `echo` or `print`, always use `return` where needed.

## Brackets
- Opening bracket MUST be on the same row as it's statement.

**Good**

```php
<?php
if ($foo) {
	bar($baz);
}
```

**Bad**

```php
<?php
if ($foo)
{
	bar($baz);
}
```
