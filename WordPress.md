[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt

# Afonso WordPress Best Practices
---

This document serves as a guideline for developers working with and for Afonso. All code delivered to Afonso MUST follow this guide. Third party libraries are excluded from this guide.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119][].

## WordPress Title
All WordPress themes SHOULD use `add_theme_support()` in the functions.php file in order to support `title tag`, instead of using `wp_title()` directly in header.php. _Example below_

**Good** (functions.php)

```php
<?php
function theme_slug_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );
```

**Bad** (header.php)

```php
<title><?php echo wp_title(); ?></title>
```
