[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt

# Afonso Markup Style Guide and Best Practices
---

This document serves as a guideline for developers working with and for Afonso. All code delivered to Afonso MUST follow this guide. Third party libraries are excluded from this guide.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119][].



## Indentation

All code MUST be indentated with hard tabs.